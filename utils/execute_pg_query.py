from psycopg2.extras import DictCursor, RealDictCursor, NamedTupleCursor
import psycopg2 as pg
import logging.config as log_config
import logging
import os

log_config.fileConfig('log.ini')
LOGGER = logging.getLogger(__name__)


def execute_pg_query(db_name="realiti", user="postgres", password="postgres", db_host="localhost", db_port="5432", query=None, query_file=None, return_results=False):
    try:
        with pg.connect(dbname=db_name, user=user, password=password, host=db_host, port=db_port) as conn:
            # cursor_factory=DictCursor/RealDictCursor/NamedTupleCursor
            with conn.cursor(cursor_factory=NamedTupleCursor) as cursor:
                if query:
                    cursor.execute(query)
                    conn.commit()
                elif query_file:
                    query_file = os.path.normpath(query_file)
                    with open(query_file, 'r') as f:
                        query_file_content = f.read()
                        cursor.execute(query_file_content)
                        conn.commit()
                if return_results:
                    res = cursor.fetchall()
                    return res
    except pg.Error as e:
        LOGGER.error("Error in dealing with the database.")
        LOGGER.error(f"pg.Error ({e.pgcode}): {e.pgerror}")
        LOGGER.error(str(e))
    except pg.Warning as w:
        LOGGER.warning("Warning from the database.")
        LOGGER.warning(f"pg.Warning: {str(w)}")
