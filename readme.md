# setup venv
`python -m venv venv`
# enable venv
on linux
`source venv/bin/activate`
on win (powershell)
`.\venv\Scripts\activate`
# setup requirements
`pip install -r .\requirements.txt`
# run script 
`python .\sql.py`
