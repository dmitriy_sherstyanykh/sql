from utils.execute_pg_query import execute_pg_query

import datetime
import random

start_date = datetime.datetime(2021, 12, 6)
day_delta = datetime.timedelta(days=1)
days_count = 1
end_date = start_date - days_count*day_delta
query_range = 1


def load_messages(file: str = None, query: str = None):
    accounts = execute_pg_query(query=query, return_results=True)
    with open(file) as query:
        cur_query = query.read()
        for i in range((start_date - end_date).days):
            for account in accounts:
                cur_date = start_date - i*day_delta
                value_amount = str(random.randint(-100, 100))
                dr_cr = 'DR' if int(value_amount) < 0 else 'CR'
                executable_query = cur_query.replace('<currency>', account.currency_code) \
                    .replace('<label_id>', account.lb_id) \
                    .replace('<nostro_id>', account.id) \
                    .replace('<value_amount>', value_amount) \
                    .replace('<dr_cr>', dr_cr) \
                    .replace('<cur_date>', str(cur_date.strftime("%Y-%m-%d"))) \
                    .replace('<query_range>', str(query_range))
                execute_pg_query(query=executable_query)


if __name__ == '__main__':
    # uncomment if you need specific accounts
    # labels = ['AUD1', 'AUD2', 'AUD3', 'AUD4', 'AUD5', 'AUD6', 'AUD7', 'AUD8', 'AED1', 'AED2', 'AED3', 'AED4', 'AED5', 'AED6', 'AED7']

    # this query used with variable 'labels' and return id, lb_id and currency_code for specifyed labels
    # query = f"""select id, lb_id, currency_code from nostro where label in ({",".join(f"'{item}'" for item in labels)})"""

    query = "select id, lb_id, currency_code from realiti.public.nostro"
    load_messages(file="sql_requests/create_statements.sql", query=query)
    load_messages(file="sql_requests/create_ledgers.sql", query=query)
