insert into matchable_message (
  id,
  body,
  original_body,
  created,
  message_type,
  is_cancelled,
  value_amount,
  value_date,
  transaction_timestamp,
  version_id,
  nostro_id,
  is_current,
  match_id,
  currency_code,
  effective_date_time
)
select
    t.id as id,
    jsonb_build_object('ID', t.id, 'ccy', t.currency_code, 'type', 'realiti', 'msgId', t.id, 'groupId', version_id, 'msgType', 'INTERNAL_LEDGER_CASHFLOW', 'drCrMark', t.dr_cr, 'extAccId', t.label, 'isSettled', 'false', 'valueDate', t.value_date, 'outputTime', t.transaction_timestamp, 'isProjected', 'true', 'valueAmount', t.value_amount, 'transactionTs', t.transaction_ts, 'extAccUniqueId', t.label, 'tradeReference', null, 'commonIntCashflowId', t.version_id, 'transactionReference', t.version_id) as body,
    jsonb_build_object('ID', t.id, 'ccy', t.currency_code, 'type', 'realiti', 'msgId', t.id, 'groupId', version_id, 'msgType', 'INTERNAL_LEDGER_CASHFLOW', 'drCrMark', t.dr_cr, 'extAccId', t.label, 'isSettled', 'false', 'valueDate', t.value_date, 'outputTime', t.transaction_timestamp, 'isProjected', 'true', 'valueAmount', t.value_amount, 'transactionTs', t.transaction_ts, 'extAccUniqueId', t.label, 'tradeReference', null, 'commonIntCashflowId', t.version_id, 'transactionReference', t.version_id) as original_body,
    now() as created,
    'ledger' as message_type,
    false as is_cancelled,
    t.value_amount as value_amount,
    t.value_date as value_date,
    t.transaction_timestamp as transaction_timestamp,
    t.version_id as version_id,
    t.nostro_id as nostro_id,
    true as is_current,
    NULL as match_id,
    t.currency_code as currency_code,
    NULL as effective_date_time
from (
	select 
	  uuid_generate_v4() as id,
	  uuid_generate_v4() as version_id,
	  '<currency>' as currency_code,
    '<label_id>' as label,
    '<nostro_id>'::uuid as nostro_id,
	  '<value_amount>'::numeric as value_amount,
    '<dr_cr>' as dr_cr,
	  '<cur_date>'::date as value_date,
	  now() as transaction_timestamp,
	  extract(epoch from now())::integer as transaction_ts
	from generate_series(1, <query_range>)
) t
