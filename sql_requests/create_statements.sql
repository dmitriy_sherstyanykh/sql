insert into matchable_message (
  id,
  body,
  original_body,
  created,
  message_type,
  is_cancelled,
  value_amount,
  value_date,
  transaction_timestamp,
  version_id,
  nostro_id,
  is_current,
  match_id,
  currency_code,
  effective_date_time
)
select
    t.id as id,
    jsonb_build_object('ID', t.id, 'ccy', t.currency_code, 'type', 'MANUAL_CASHFLOW', 'msgTs', t.transaction_timestamp, 'msgType', 'Cashflow', 'drCrMark', t.dr_cr, 'extAccId', t.label, 'isDefund', 'false', 'inputTime', t.transaction_timestamp, 'senderBic', null, 'valueDate', t.value_date, 'cashflowId', t.id, 'outputTime', t.transaction_timestamp, 'is_rejected', false, 'processedTs', t.transaction_timestamp, 'receiverBic', null, 'sendRecInfo', null, 'valueAmount', t.value_amount, 'effectiveTime', t.effective_time, 'extAccUniqueId', t.label, 'refForAccOwner', null, 'commonIntCashflowId', t.version_id, 'refAccServicingInst', 'xxx') as body,
    jsonb_build_object('ID', t.id, 'ccy', t.currency_code, 'type', 'MANUAL_CASHFLOW', 'msgTs', t.transaction_timestamp, 'msgType', 'Cashflow', 'drCrMark', t.dr_cr, 'extAccId', t.label, 'isDefund', 'false', 'inputTime', t.transaction_timestamp, 'senderBic', null, 'valueDate', t.value_date, 'cashflowId', t.id, 'outputTime', t.transaction_timestamp, 'is_rejected', false, 'processedTs', t.transaction_timestamp, 'receiverBic', null, 'sendRecInfo', null, 'valueAmount', t.value_amount, 'effectiveTime', t.effective_time, 'extAccUniqueId', t.label, 'refForAccOwner', null, 'commonIntCashflowId', t.version_id, 'refAccServicingInst', 'xxx') as original_body,
    now() as created,
    'cashflow' as message_type,
    false as is_cancelled,
    t.value_amount as value_amount,
    t.value_date as value_date,
    t.transaction_timestamp as transaction_timestamp,
    t.version_id as version_id,
    t.nostro_id as nostro_id,
    true as is_current,
    NULL as match_id,
    t.currency_code as currency_code,
    t.effective_time as effective_date_time
from (
	select 
	  uuid_generate_v4() as id,
	  uuid_generate_v4() as version_id,
	  '<currency>' as currency_code,
    '<label_id>' as label,
    '<nostro_id>'::uuid as nostro_id,
	  '<value_amount>'::numeric as value_amount,
    '<dr_cr>' as dr_cr,
	  '<cur_date>'::date as value_date,
	  '<cur_date>T16:00:00'::date as effective_time,
	  now() as transaction_timestamp,
	  extract(epoch from now())::integer as transaction_ts
	from generate_series(1, <query_range>)
) t
